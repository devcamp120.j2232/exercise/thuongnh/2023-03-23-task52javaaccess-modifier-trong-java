import java.util.ArrayList;

import donggoi.Person;


public class App {

    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // khởi tạo mảng có kiểu dữ liệu là Person
        ArrayList<Person> arrListThuong = new ArrayList<>();
        // khởi tạo đối tượng  peson với các tham số khác nhau 
        Person person0 = new Person();
        Person person1 = new Person("Devcamp");
        Person person2 = new Person("thuong", 24, 60.6 );
        Person person3 = new Person("chó", 30,68.8,5000000, new String[] {"cho", "meo"});

        // thêm các object vào danh sách 
        arrListThuong.add(person0);
        arrListThuong.add(person1);
        arrListThuong.add(person2);
        arrListThuong.add(person3);
        // in ra màn hình 
        for (Person person   : arrListThuong){
            System.out.println(person.toString());
        }
    }
}
