package donggoi;

import java.util.Arrays;

public class Person {

    
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return  " [" + name + "," + age  + "," + weight + ","  + salary + "," +  Arrays.toString(pets) + "]";
    }
    String name;
    int age;
    double weight;
    long salary;
    String[] pets;
    // khỏi tạo với 1 tham số 
    public Person(String name){
        this.name = name;
        this.age = 18;
        this.weight = 60.6;
        this.salary = 1000000;
        this.pets = new String []  {"Big dog", "Small cat", "Gold Fish" };
    }
    // khơi tạo với tất cả các tham số 
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }
     // khởi tạo không tham số 
     public Person() {
        this("thuongnh");
       
    }
    // khơi tao 3 tham số 
    public Person(String name, int age, double weight) {
        // this.name = name;
        // this.age = age;
        // this.weight = weight;
        this(name, age, weight,200000, new String[] {"Big dog", "Small cat", "Gold Fish" });
    }
   
    


}
